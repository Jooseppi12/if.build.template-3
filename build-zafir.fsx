#load "tools/includes.fsx"
open IntelliFactory.Build

let bt =
    BuildTool().PackageId("ZPROJECTNAME")
        .VersionFrom("Zafir")
        .WithFSharpVersion(FSharpVersion.FSharp30)
        .WithFramework(fun f -> f.Net40)

let main =
    bt.Zafir.PROJECTTYPE("PROJECTNAME")
        .SourcesFromProject()
        .Embed([])
        .References(fun r -> [])

let tests =
    bt.Zafir.SiteletWebsite("PROJECTNAME.Tests")
        .SourcesFromProject()
        .Embed([])
        .References(fun r ->
            [
                r.Project(main)
                r.NuGet("Zafir.Testing").Latest(true).Reference()
                r.NuGet("Zafir.UI.Next").Latest(true).Reference()
            ])

bt.Solution [
    main
    tests

    bt.NuGet.CreatePackage()
        .Configure(fun c ->
            { c with
                Title = Some "PROJECTTITLE"
                LicenseUrl = Some "http://websharper.com/licensing"
                ProjectUrl = Some "https://github.com/intellifactory/PROJECTREPO"
                Description = "PROJECTDESCR"
                RequiresLicenseAcceptance = true })
        .Add(main)
]
|> bt.Dispatch
